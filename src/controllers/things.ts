import {Request, Response} from 'express';
import {
  hasAuth,
  isDebugging,
  isMethodImplemented,
  sanitizeBody,
  shouldAuthorize,
  validateAuth,
  validatePostRequest,
} from '../helpers';

const index = (req: Request, res: Response) => {
  try {
    if (!isMethodImplemented(req.method)) {
      return res.status(415).send(null);
    }
    if (shouldAuthorize(req)) {
      if (!hasAuth(req)) {
        return res.status(401).send(null);
      }
      validateAuth(req);
    }
    switch (req.method) {
      case 'POST': {
        validatePostRequest(req);
        if (isDebugging(req)) {
          return res.status(200).send({message: 'Hello debug!'});
        }
        const {name} = sanitizeBody(req.body);
        return res
          .status(201)
          .send({
            message: `Hello ${name}!`,
          });
      }
      case 'DELETE': {
        const {id} = req.params;
        if (!id) {
          // Even though we are handling, we must use an id (captured in another handler definition in app.js);
          return res.status(405).send(null);
        }
        return res.status(200).send({
          message: `Goodbye ${id}!`,
        });
      }
      default:
        return res
          .status(200)
          .send({
            message: 'Hello',
          });
    }
  } catch (e) {
    if (/Illegal token/.test(e.message)) {
      return res.status(403).send(null);
    }
    if (/Unprocessable .*/.test(e.message)) {
      return res.status(422).send({
        message: 'Body must be a single "name" parameter',
      });
    }
    return res.status(500).send({
      message: 'Unknown Error',
    });
  }
};

export {index};
