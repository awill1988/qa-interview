import 'jest';
import request from 'supertest';
import app from '../app';

describe('GET /things', () => {
  it('should return 200 OK', () => {
    return request(app).get('/things')
      .expect(200);
  });
});

describe('DELETE /things/:id', () => {
  it('should return 401 when no auth', () => {
    return request(app).delete('/things')
      .expect(401);
  });
});

describe('POST /things', () => {
  it('should return 401 when no auth', () => {
    return request(app).post('/things')
      .expect(401);
  });
});

describe('PUT /things', () => {
  it('should return 415 for not implemented method', () => {
    return request(app).put('/things')
      .expect(415);
  });
});
