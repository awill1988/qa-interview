import {Request} from 'express';

const IMPLEMENTED_METHODS = ['GET', 'POST', 'DELETE'];

/**
 * Checks if the request has debugging mode
 * @param request
 */
export function isDebugging(request: Request) {
  const debugHeader = request.header('X-Debug');
  let debugging = false;
  if (debugHeader === 'true') {
    debugging = true;
  }
  return debugging;
}

/**
 * Checks if the request has an authorization or api token
 * @param request
 */
export const hasAuth = (request: Request) => {
  return request.header('X-Api-Key') ||
    (request.header('Authorization') && /Bearer .*/.test(request.header('Authorization') as string));
};

/**
 * Returns a sanitized body
 * @param payload
 */
export const sanitizeBody = (payload: any): any => {
  let temp;
  if (typeof payload !== 'string') {
    return JSON.parse(JSON.stringify(payload));
  }
  try {
    temp = JSON.parse(payload);
    if (typeof temp !== 'object' || temp === null) {
      throw new Error('Not a valid payload');
    }
    return temp;
  } catch (e) {
    throw new Error('Not a valid payload');
  }
};

/**
 * Returns true if the HTTP request method is not GET
 * @param request
 */
export const shouldAuthorize = (request: Request): boolean => {
  return ['GET'].find((m) => request.method === m) === undefined;
};

/**
 * Returns true if the method is supported
 * @param method
 */
export const isMethodImplemented = (method: string): boolean => {
  return IMPLEMENTED_METHODS.find((m) => method === m) !== undefined;
};

/**
 * Returns void (undefined) when valid auth exists. Otherwise, throw an error
 * @param request
 */
export const validateAuth = (request: Request): void => {
  let auth = request.header('X-Api-Key');
  if (!auth) {
    auth = request.header('Authorization');
    if (!auth) {
      throw new Error ('No Auth on the request!');
    }
    auth = auth.replace('Bearer ', '');
  }
  if (auth !== (process.env.AUTH_TOKEN || 'secret')) {
    throw new Error('Illegal token');
  }
  return;
};

/**
 * Returns void (undefined) when expected parameters exist
 * @param body
 */
export const validatePostRequest = (request: Request): void => {
  const body = sanitizeBody(request.body);
  if (Object.keys(body).length === 1 && request.method === 'POST' && body.name) {
    return;
  }
  throw new Error('Unprocessable entity');
};
