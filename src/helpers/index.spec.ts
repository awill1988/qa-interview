import 'jest';
import * as Helpers from './';

/**
 * This is another way to mock the request. You don't need to do it this way.
 * Do what is comfortable for you.
 * @param headers
 */
const mockedRequest = (headers: {[key: string]: string}) => {
  return {
    header(name: string) {
      const [, value]: any = (Object.entries(headers).find(([key]) => {
        return key.trim().toLowerCase() === name.trim().toLowerCase();
      }) || []);
      return value;
    },
  };
};

describe('Helper functions', () => {
  it('should check for debug', () => {
    let request: any = mockedRequest({'X-Debug': 'false'});
    expect(Helpers.isDebugging(request)).toBeFalsy();
    request = mockedRequest({'X-Debug': 'true'});
    expect(Helpers.isDebugging(request)).toBeTruthy();
  });
});
