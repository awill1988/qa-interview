import express from 'express';
import bodyParser from 'body-parser';
import path from 'path';

// Controllers (route handlers)
import * as thingsController from './controllers/things';

// Create Express server
const app = express();

// Express configuration
app.set('port', process.env.PORT || 3010);
// First assume json with applicaiton/json header type
app.use(bodyParser.json({limit: '200b'}));
// Otherwise accept json as text (without proper headers)
app.use(bodyParser.text({defaultCharset: 'utf-8', limit: '200b'}));
app.use(
  express.static(path.join(__dirname, 'public'), {maxAge: 31557600000}),
);

app.all('/things', thingsController.index);
app.delete('/things/:id', thingsController.index);

export default app;
