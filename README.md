# QA Interview

#### Overview
Demonstrate how to expand code coverage for an express web server using the following toolchain:
- NodeJS
- Typescript (superset of javascript with typing)
- TSLint (static code analysis)
- Jest (testing framework)

#### Prerequisites & Setup
1. Install latest (version 12) of NodeJS on your machine 
2. If you have Windows, use MinGW or Git for Windows bash to issue commands in this guide
3. Verify you can install all packages using a terminal `npm install`
4. Verify you can start the express server by opening a terminal and issuing `npm start`

#### TL;DR;
For example to install and get going after you installed NodeJs, issue the following commands in your terminal

```bash
cd {PROJECT_DIRECTORY};
npm install;
npm run watch-test;
```

You will see output for code coverage.

#### Tasks
1. Pick-up and expand unit tests for helper functions, ensuring 90% code coverage, but feel
free to go for 100% (`src/helpers/index.spec.ts`)
2. Pick-up and expand integration test for the server (`src/controllers/things/things.spec.ts`)
3. Deliver your solution to interviewer in one of 3 ways:
    - Publicly available remote git repo (e.g. Github.com, Gitlab.com, etc.)
    - via a merge request on existing repo (aka Peer Review for github users)
    - zipping up and sending via email to interviewers
4. Demonstrate solution with interviewers
