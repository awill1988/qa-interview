module.exports = {
  verbose: true,
  rootDir: 'src',
  transform: {
    '^.+\\.tsx?$': 'ts-jest'
  },
  setupFilesAfterEnv: ['<rootDir>/__tests__/setup.ts'],
  globals: {
    'ts-jest': {
      babelConfig: null,
      diagnostics: true,
      tsConfig: 'tsconfig.test.json'
    }
  },
  moduleFileExtensions: [
    'ts',
    'tsx',
    'js',
    'jsx',
    'json',
    'node'
  ],
  testRegex: `.*/.*\\.${process.env.INTEGRATION_TESTS === '1' ? '[i]' : ''}spec\\.(ts)$`,
  collectCoverage: false,
  coverageDirectory: '<rootdir>/../../coverage',
  coverageThreshold: {
    global: {
      branches: 90,
      functions: 90,
      lines: 90,
      statements: 90,
    }
  }
};
